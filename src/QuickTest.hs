{-# LANGUAGE RecursiveDo, ScopedTypeVariables, FlexibleContexts, TypeFamilies, ConstraintKinds, TemplateHaskell #-}
module QuickTest where

import Prelude hiding (mapM, mapM_, all, sequence)

import GHCJS.DOM.Element
import Control.Monad hiding (mapM, mapM_, forM, forM_, sequence)
import Control.Monad.Trans
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Foldable
import Data.Monoid ((<>))
import Data.List (intercalate)
import Data.FileEmbed
import Control.Concurrent
import qualified Data.Text as T

import Reflex
import Reflex.Dom

main :: IO ()
main = mainWidgetWithCss $(embedFile "style.css") todoMVC

todoMVC :: MonadWidget t m => m ()
todoMVC = do
  el "div" $ text "Hello world"

